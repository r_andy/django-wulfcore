from django.contrib.auth.admin import UserAdmin
from django.contrib import admin

from wulf import models


class StatusUpdateMixin:
    """ Update Status of model from for subclasses of ResourceStatusMixin
    """
    status_fieldset = ('Status Info', {'fields': ['status', 'created', 'updated', 'created_by', 'updated_by']})
    status_fieldset_name = 'Status Info'
    status_readonly_fields = ('created', 'updated', 'created_by', 'updated_by')
    empty_value_display = '-none-'

    def save_model(self, request, obj, form, change):
        obj.updated_by = request.user
        if not change:
            obj.created_by = request.user
        return super().save_model(request, obj, form, change)

    def get_fieldsets(self, request, obj=None):
        if not self.fieldsets[-1][0] == self.status_fieldset_name:
            return self.fieldsets + (self.status_fieldset, )
        return self.fieldsets

    def get_readonly_fields(self, request, obj=None):
        if set(self.status_readonly_fields) - set(self.readonly_fields):
            fields = set(self.readonly_fields)
            fields.update(self.status_readonly_fields)
            return tuple(fields)
        return self.readonly_fields


@admin.register(models.WulfUser)
class WulfUserAdmin(StatusUpdateMixin, UserAdmin):
    """ User model admin
    """
    list_display = ('email', 'access_level', 'status', 'created')
    ordering = ('email', 'status', 'created')
    list_filter = ('access_level', 'status')
    readonly_fields = ('user_id', 'password', 'is_staff', 'is_admin', 'is_active')
    fieldsets = (
        (None, {'fields': ['user_id', 'email', 'access_level', 'password']}),
        ('Authorization', {'fields': ['is_active', 'is_staff', 'is_admin']})
    )


@admin.register(models.Artist)
class ArtistAdmin(StatusUpdateMixin, admin.ModelAdmin):
    """ Artist model admin
    """
    list_display = ('name', 'country', 'birth_year', 'status', 'created')
    list_filter = ('country', 'status')
    ordering = ('name', 'status', 'birth_year', 'country', 'status')
    readonly_fields = ('artist_id', )
    fieldsets = (
        (None, {'fields': ['artist_id', 'name', 'country', 'birth_year']}),
    )
