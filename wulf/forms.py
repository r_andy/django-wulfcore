""" Forms for general user activities
"""
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import PasswordResetForm

from wulf.models import Status, Artist


class WulfPasswordResetForm(PasswordResetForm):
    """ Override
    """
    def get_users(self, email):
        User = get_user_model()
        return User.objects.filter(email__iexact=email, status=Status.active.value)
