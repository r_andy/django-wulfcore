""" Identity provider urls
"""
from django.urls import reverse_lazy
from django.conf.urls import url
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView

from wulf import views, forms
from wulf.sp_urls import urlpatterns

urlpatterns += [
    # SSO provider stuff
    url(r'^sso/start/$', views.idp_sso_start, name='sso-start'),
    url(r'^sso/auth/$', views.idp_sso_auth, name='sso-auth'),
    url(r'^sso/form/auth/$', views.idp_sso_form_auth, name='sso-form-auth'),
    url(r'^sso/finish/$', views.idp_sso_finish, name='sso-finish'),
    # Password reset stuff
    url(r'^sso/password/reset/$',
        PasswordResetView.as_view(
            form_class=forms.WulfPasswordResetForm,
            success_url=reverse_lazy('wulf:sso-password-reset-finish'),
            email_template_name='wulf/password-reset-email.txt',
            template_name='wulf/reset-password.html'
        ),
        name='sso-password-reset'),
    url(r'^sso/password/reset/finish/$',
        PasswordResetDoneView.as_view(
            template_name='wulf/reset-password-finish.html'
        ),
        name='sso-password-reset-finish'),
    url(r'^sso/password/reset/confirm/(?P<uidb64>[\w_-]+)/(?P<token>[\w_-]+)/$',
        PasswordResetConfirmView.as_view(
            success_url=reverse_lazy('wulf:sso-auth'),
            template_name='wulf/reset-password-confirm.html'
        ),
        name='sso-password-reset-confirm'),
    # User API GET endpoint
    url(r'^api/user/(?P<uid>\w+)/$', views.user_info, name='user-info'),
]
