""" JWT library
"""
import base64
import binascii
from collections import namedtuple
from typing import NamedTuple
from operator import methodcaller

import ujson

from django.conf import settings
from django.utils import timezone
from django.utils.encoding import smart_bytes

from wulf import signing


DATETIME_RESOLUTION = 10 ** 6


class JWTException(NamedTuple):
    message: str


class StdAbbr:
    """ standard abbreviations
    """
    algorithm = 'alg'
    type = 'typ'
    subject = 'sub'
    issuer = 'iss'
    not_before = 'nbf'
    not_after = 'naf'


def pack_json(data):
    return ujson.dumps(data)


def unpack_json(data):
    return ujson.loads(data)


def pack_jwt_b64(data):
    data = smart_bytes(data)
    return base64.urlsafe_b64encode(data).replace(b'=', b'').decode()


def unpack_jwt_b64(data):
    data = smart_bytes(data)
    rem = 4 - (len(data) % 4)
    data += b'=' * rem
    return base64.urlsafe_b64decode(data)


def pack_jwt(data, *, subject, expires=timezone.timedelta(days=365)):
    """ pack this shit into a jwt
    """
    now = timezone.now()
    head = ujson.dumps({
        StdAbbr.algorithm: 'ES256',
        StdAbbr.type: 'jwt',
        StdAbbr.subject: subject,
        StdAbbr.issuer: settings.WULF_COMMON_NAME,
        StdAbbr.not_before: pack_datetime(now),
        StdAbbr.not_after: pack_datetime(now + expires)
    })
    body = ujson.dumps(data)
    sign_data = b'.'.join(map(methodcaller('encode'), (head, body)))
    signature = signing.sign(sign_data)
    return '.'.join(map(pack_jwt_b64, (head, body, signature)))


def _unpack_jwt(jwt):
    """ unpack a jwt into its component parts
    """
    head, body, signature = map(unpack_jwt_b64, jwt.split('.', 3))
    return (
        ujson.loads(head.decode()),
        ujson.loads(body.decode()),
        b'.'.join((head, body)),
        signature
    )


def pack_datetime(dt):
    """ Pack a UTC date time into an integer timestamp
    """
    return int(dt.timestamp() * DATETIME_RESOLUTION)


def unpack_timestamp(ts):
    """ Unpack an integer timestamp into a UTC date time
    """
    return timezone.datetime.fromtimestamp(ts / DATETIME_RESOLUTION).replace(tzinfo=timezone.utc)


def _to_namedtuple(name, data):
    cls = namedtuple(name, data.keys())
    return cls(**data)


def unpack_jwt(jwt):
    """ Verify a jwt
    """
    try:
        head, body, signed_data, signature = _unpack_jwt(jwt)
    except (ValueError, binascii.Error):
        return None, None, JWTException(message='Invalid JWT Format')

    getitem = head.get

    issuer = getitem(StdAbbr.issuer)
    if not issuer:
        return head, body, JWTException(message='Unknown issuer')

    subject = getitem(StdAbbr.subject)
    if subject != settings.WULF_COMMON_NAME:
        return head, body, JWTException(message='Subject does not match common name')

    now = timezone.now()
    not_before = getitem(StdAbbr.not_before)
    if not_before and unpack_timestamp(not_before) > now:
        return head, body, JWTException(message='JWT cannot yet be used')
    not_after = getitem(StdAbbr.not_after)
    if not_after and now > unpack_timestamp(not_after):
        return head, body, JWTException(message='JWT has expired')

    try:
        pub = signing.CertStore.load(issuer).public_key()
    except FileNotFoundError:
        return head, body, JWTException(message='Unknown issuer')

    if not signing.verify(signature, signed_data, pub):
        return head, body, JWTException(message='Invalid signature')

    return _to_namedtuple('JWTHead', head), _to_namedtuple('JWTBody', body), None
