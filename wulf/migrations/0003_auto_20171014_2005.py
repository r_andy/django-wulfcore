# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-14 20:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wulf', '0002_auto_20171014_1926'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artist',
            name='birth_year',
            field=models.PositiveSmallIntegerField(default=1900),
        ),
        migrations.AlterField(
            model_name='artist',
            name='country',
            field=models.CharField(default='US', max_length=2),
        ),
        migrations.AlterField(
            model_name='artist',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(0, 'active'), (1, 'pending'), (2, 'suspended'), (3, 'deleted')], default=1),
        ),
        migrations.AlterField(
            model_name='wulfuser',
            name='access_level',
            field=models.PositiveSmallIntegerField(choices=[(0, 'basic'), (1, 'staff'), (2, 'admin')], default=0),
        ),
        migrations.AlterField(
            model_name='wulfuser',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(0, 'active'), (1, 'pending'), (2, 'suspended'), (3, 'deleted')], default=1),
        ),
    ]
