""" Sign certificate signing request
"""
import os.path

from cryptography import x509
from cryptography.hazmat.backends import default_backend

from django.conf import settings
from django.core.management.base import BaseCommand

from wulf import signing


class Command(BaseCommand):
    """ Generate IDP certs
    """
    help = __doc__

    def add_arguments(self, parser):
        parser.add_argument('-i', '--infile', required=True, help='input file')

    def handle(self, *args, **options):
        with open(options['infile'], 'rb') as fo:
            csr = x509.load_pem_x509_csr(
                fo.read(),
                backend=default_backend()
            )
        signing.sign_csr(csr)
