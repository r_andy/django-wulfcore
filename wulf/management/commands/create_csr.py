""" Generate certificate signing request as well as public and private key pair
"""
import os.path

from django.conf import settings
from django.core.management.base import BaseCommand

from wulf import signing


class Command(BaseCommand):
    """ Generate IDP certs
    """
    help = __doc__

    def handle(self, *args, **options):
        signing.create_csr()
