from collections import namedtuple

from django.conf import settings

def make_config():
    keys = {}
    for key, value in settings.__dict__:
        if key.startswith('WULF_'):
            keys[key.lower()] = value
    cls = namedtuple('WulfConfig', keys.keys())
    return cls(**keys)


def get_config(name):
    return getattr(config, name.lower(), None)

config = make_conifg()
