"""
"""
import os
from datetime import datetime, timedelta

from cryptography import x509
from cryptography.x509.oid import NameOID

from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import ec

from django.conf import settings
from django.utils.encoding import smart_bytes

CERT_PATH = os.path.join(settings.WULF_CERT_DIR, settings.WULF_COMMON_NAME + '.cert')
CSR_PATH = os.path.join(settings.WULF_CERT_DIR, settings.WULF_COMMON_NAME + '.csr')
PEM_PATH = os.path.join(settings.WULF_CERT_DIR, settings.WULF_COMMON_NAME + '.pem')
PUB_PATH = os.path.join(settings.WULF_CERT_DIR, settings.WULF_COMMON_NAME + '.pub')
IDP_PATH = os.path.join(settings.WULF_CERT_DIR, settings.WULF_IDP_NAME + '.pub')

PEM = None
PUB = None
CERT = None
IDP = None


def get_private_key():
    """ Load a pem private key
    """
    global PEM
    if PEM is None:
        with open(PEM_PATH, 'rb') as fo:
            PEM = serialization.load_pem_private_key(
                fo.read(),
                password=settings.WULF_PEM_PASSWORD,
                backend=default_backend()
            )
    return PEM


def get_public_key():
    """ Load a pem public key
    """
    global PUB
    if PUB is None:
        with open(PUB_PATH, 'rb') as fo:
            PUB = serialization.load_pem_public_key(
                fo.read(),
                backend=default_backend()
            )
    return PUB


def get_cert():
    """ Load a pem certificate
    """
    global CERT
    if CERT is None:
        with open(CERT_PATH, 'rb') as fo:
            CERT = x509.load_pem_x509_certificate(
                fo.read(),
                backend=default_backend()
            )
    return CERT


def get_idp_public_key():
    """ Load a pem public key for identity provider
    """
    global IDP
    if IDP is None:
        with open(IDP_PATH, 'rb') as fo:
            IDP = serialization.load_pem_public_key(
                fo.read(),
                backend=default_backend()
            )
    return IDP


def sign(message):
    """ Sign a message
    """
    pem = get_private_key()
    return pem.sign(
        smart_bytes(message),
        ec.ECDSA(hashes.SHA256()),
    )


def verify(signature, message, pub):
    """ verify a signature
    """
    try:
        pub.verify(signature, message, ec.ECDSA(hashes.SHA256()))
        return True
    except InvalidSignature:
        return False


def create_csr():
    """ Create a certificate signing request
    """
    try:
        pem = get_private_key()
    except FileNotFoundError:
        pem = ec.generate_private_key(ec.SECP384R1(), default_backend())
        with open(PEM_PATH, 'wb') as fo:
            serialized_pem = pem.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.PKCS8,
                encryption_algorithm=serialization.NoEncryption()
            )
            fo.write(serialized_pem)

    try:
        pub = get_public_key()
    except FileNotFoundError:
        pub = pem.public_key()
        with open(PUB_PATH, 'wb') as fo:
            serialized_pem = pub.public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo
            )
            fo.write(serialized_pem)

    csr = x509.CertificateSigningRequestBuilder().subject_name(x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, 'US'),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, 'CA'),
        x509.NameAttribute(NameOID.LOCALITY_NAME, 'Los Angeles'),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, 'the wulf.'),
        x509.NameAttribute(NameOID.COMMON_NAME, settings.WULF_COMMON_NAME),
    ]))

    csr = csr.sign(pem, hashes.SHA256(), default_backend())
    with open(CSR_PATH, 'wb') as fo:
        fo.write(csr.public_bytes(serialization.Encoding.PEM))
    return csr


def sign_csr(csr):
    pem = get_private_key()
    issuer = x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, 'US'),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, 'CA'),
        x509.NameAttribute(NameOID.LOCALITY_NAME, 'Los Angeles'),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, 'the wulf.'),
        x509.NameAttribute(NameOID.COMMON_NAME, settings.WULF_IDP_NAME),
    ])
    builder = x509.CertificateBuilder()
    builder = builder.issuer_name(issuer)
    builder = builder.subject_name(csr.subject)
    builder = builder.public_key(csr.public_key())
    builder = builder.serial_number(x509.random_serial_number())
    builder = builder.not_valid_before(datetime.utcnow())
    builder = builder.not_valid_after(datetime.utcnow() + timedelta(days=365))

    cert = builder.sign(pem, hashes.SHA256(), default_backend())
    common_name = cert.subject.get_attributes_for_oid(NameOID.COMMON_NAME)[0].value

    with open(os.path.join(settings.WULF_CERT_DIR, common_name + '.cert'), 'wb') as fo:
        fo.write(cert.public_bytes(serialization.Encoding.PEM))


class CertStore:
    """ Caching certs loaded from disk
    """
    _data = {}

    def __new__(*args, **kwargs):
        # singleton class, no instance, just one floating class object
        return CertStore

    @classmethod
    def load(cls, name):
        cert = cls._data.get(name)
        if cert is None:
            with open(os.path.join(settings.WULF_CERT_DIR, name + '.cert'), 'rb') as fo:
                cert = x509.load_pem_x509_certificate(
                    fo.read(),
                    backend=default_backend()
                )
            cls._data[name] = cert
        return cert
