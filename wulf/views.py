""" SSO and User API endpoints
"""
from django.conf import settings
from django.urls import reverse
from django.utils import timezone
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.forms import AuthenticationForm
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods, require_POST, require_GET

from wulf import jwt

SSO_COMPLETED_COOKIE = 'wulf.sso.complete'


@csrf_exempt
@require_http_methods(['GET', 'POST'])
def idp_sso_start(request):
    if request.method == 'GET':
        authn = request.GET.get('authn')
    else:
        authn = request.POST.get('authn')

    if not (authn or request.session.get('authn_data')):
        message = f'Missing request data.'
        return render(request, 'wulf/autherr.html', context={'message': message}, status=400)

    if authn:
        head, body, exc = jwt.unpack_jwt(authn)
        if exc is not None:
            message = exc.message
            return render(request, 'wulf/autherr.html', context={'message': message}, status=400)
        
        data = {'loc': body.loc, 'bin': body.bin, 'iss': head.iss}
        request.session['authn_data'] = jwt.pack_json(data)

    return redirect(reverse('wulf:sso-auth'))


@require_GET
def idp_sso_auth(request, **kwargs):
    if request.user.is_authenticated():
        return redirect(reverse('wulf:sso-finish'))
    return render(request, 'wulf/formauth.html', context={'form': AuthenticationForm()})


@require_POST
def idp_sso_form_auth(request):
    """ login using credentials or google account
    """
    User = get_user_model()
    if not request.user.is_authenticated():
        form = AuthenticationForm(data=request.POST)
        if not form.is_valid():
            return render(request, 'wulf/formauth.html', context={'form': form})
        login(request, form.get_user())
    return redirect(reverse('wulf:sso-finish'))


@require_GET
def idp_sso_finish(request, **kwargs):
    if not request.user.is_authenticated():
        return redirect(reverse('wulf:sso-auth'))

    authn_data_str = request.session.get('authn_data')
    if not authn_data_str:
        message = f'Missing state data. do you have cookies enabled?'
        return render(request, 'wulf/autherr.html', context={'message': message})

    if not request.user.is_active:
        message = f'User {request.user.email} is not an active user'
        return render(request, 'wulf/autherr.html', context={'message': message})

    authn_data = jwt.unpack_json(authn_data_str)
    context = {
        'action': authn_data['loc'],
        'method': authn_data['bin'],
        'resp': jwt.pack_jwt({'uid': request.user.pk.hex}, subject=authn_data['iss']),
        'no_csrf': True
    }
    return render(request, 'wulf/postauth.html', context=context)


@require_GET
def login_view(request, template_name='wulf/login.html'):
    """ Stub for logging in
    """
    return render(request, template_name, context={'cookie_name': SSO_COMPLETED_COOKIE})


@require_GET
def login_start(request):
    """ create authn request and post to IDP endpoint
    """
    jwt_body = {
        'loc': request.build_absolute_uri(reverse('wulf:login-finish')),
        'bin': 'POST'
    }
    context = {
        'action': settings.WULF_SSO_URL,
        'method': 'POST',
        'authn': jwt.pack_jwt(jwt_body, subject=settings.WULF_IDP_NAME),
        'no_csrf': True
    }
    return render(request, 'wulf/postauth.html', context=context)


@csrf_exempt
@require_POST
def login_finish(request):
    """ create authn request and post to IDP endpoint
    """
    jwt_str = request.POST.get('resp')
    if not jwt_str:
        message = f'Invalid state, request arguments missing'
        return render(request, 'wulf/autherr.html', context={'message': message})

    user = authenticate(request, jwt_str=jwt_str)
    if not user:
        message = f'Unable to login successfully'
        return render(request, 'wulf/autherr.html', context={'message': message})

    login(request, user)
    request.session['jwt'] = jwt_str
    request.session.set_expiry((user._session_expires - timezone.now()).total_seconds())
    response = render(request, 'wulf/login-finish.html')
    response.set_cookie(SSO_COMPLETED_COOKIE, 'ok', max_age=5)
    return response


@require_GET
def logout_view(request):
    """ log the user out
    """
    logout(request)
    response = redirect(request.GET.get('next') or reverse('wulf:login'))
    response.delete_cookie(SSO_COMPLETED_COOKIE)
    return response


@require_GET
def user_info(request, uid):
    """ A simple user info API
    """
    authn = request.META.get('HTTP_AUTHORIZATION', '').split('JWT ')[-1]
    head, body, exc = jwt.unpack_jwt(authn)
    if exc:
        response = HttpResponse(jwt.pack_json({'message': exc.message}), status=400)
        response['content-type'] = 'application/json'
        return response
    if body.uid != uid:
        message = 'Missmatched user id\'s'
        response = HttpResponse(jwt.pack_json({'message': message}), status=401)
        response['content-type'] = 'application/json'
        return response

    User = get_user_model()
    try:
        user = User.objects.get(pk=uid)
    except User.DoesNotExist:
        response = HttpResponse(jwt.pack_json({'message': 'User not found'}), status=404)
        response['content-type'] = 'application/json'
        return response
    user_dict = {
        'user_id': user.pk.hex,
        'email': user.email,
        'status': user.status,
        'access_level': user.access_level,
        'is_superuser': user.is_superuser
    }
    response = HttpResponse(jwt.pack_json(user_dict))
    response['content-type'] = 'application/json'
    return response
