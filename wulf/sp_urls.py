""" service provider urls
"""
from django.conf.urls import url

from wulf import views

urlpatterns = [
    url(r'^login/$', views.login_view, name='login'),
    url(r'^login/start/$', views.login_start, name='login-start'),
    url(r'^login/finish/$', views.login_finish, name='login-finish'),
    url(r'^logout/$', views.logout_view, name='logout')
]
