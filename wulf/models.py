""" Core models for user interaction with the wulf.

must add `AUTH_USER_MODEL = 'wulf.WulfUser'` to settings.py
"""
import hashlib
import enum
import uuid

from django.conf import settings
from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin


class ChoicesMixin:
    """ Dump enum as a list valid for model and form choices
    """
    @classmethod
    def as_choices(cls):
        return [(c.value, c.name) for c in cls]


class Status(ChoicesMixin, enum.Enum):
    """ Resource status value
    """
    active = 0
    pending = 1
    suspended = 2
    deleted = 3


class ResourceStatusMixin(models.Model):
    """ Generic resource status mixin
    """
    class Meta:
        abstract = True

    status = models.PositiveSmallIntegerField(
        choices=Status.as_choices(),
        default=Status.pending.value
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name='%(class)ss_created')
    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name='%(class)ss_updated')

    @property
    def etag_value(self):
        """ Provide etags for all resources
        """
        last_updated_bytes = self.updated.isoformat().encode('ascii')
        return hashlib.sha1(last_updated_bytes).digest().hex()

    def save(self, user=None, **kwargs):
        if user is not None:
            self.updated_by = user
            if not self.pk:  # Model is being saved for the first time
                self.created_by = user
        return super().save(**kwargs)

    def delete(self, user=None, **kwargs):
        self.status = Status.deleted.value
        self.save(user=user, **kwargs)


class WulfUserClass(ChoicesMixin, enum.Enum):
    """ Differentiating between user types
    """
    basic = 0
    staff = 1
    admin = 2


class WulfUserManager(BaseUserManager):
    """ Bare bones manager
    """
    def create_user(self, email=None, password=None, username=None):
        """ Create a normal user
        """
        if not (email or username):  # accomodating social auth
            raise ValueError('Field `email` cannot be null!')
        email = self.normalize_email(email or username)
        user = self.model(email=email)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None):
        """ Create a superuser
        """
        if not email:
            raise ValueError('Field `email` cannot be null!')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_superuser=True,
                          status=Status.active.value,
                          access_level=WulfUserClass.admin.value)
        user.set_password(password)
        user.save(using=self._db)
        return user


class WulfUser(PermissionsMixin, ResourceStatusMixin, AbstractBaseUser):
    """ The minimum viable user
    """
    user_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True
    )
    access_level = models.PositiveSmallIntegerField(
        choices=WulfUserClass.as_choices(),
        default=WulfUserClass.basic.value
    )

    USERNAME_FIELD = 'email'

    objects = WulfUserManager()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._session_expires = None

    @property
    def is_active(self):
        """ Is the user active
        """
        return self.status == Status.active.value

    @property
    def is_admin(self):
        """ User has superuser access
        """
        return self.access_level == WulfUserClass.admin.value

    @property
    def is_staff(self):
        """ User has admin access
        """
        return self.access_level > WulfUserClass.basic.value

    def email_user(self, subject, message, from_email=None, **kwargs):
        """ Send an email to this user.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_short_name(self):
        return self.email

    def __str__(self):
        return f'User - {self.email} {WulfUserClass(self.access_level).name}'


class Artist(ResourceStatusMixin):
    """ A core wulf model used across several apps
    """
    artist_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=120)
    bio = models.TextField(blank=True)
    weblink = models.URLField(blank=True)
    country = models.CharField(default='US', max_length=2)
    birth_year = models.PositiveSmallIntegerField(default=1900)

    def __str__(self):
        return f'Artist - {self.name} [{self.country}] b. {self.birth_year}'

    __repr__ = __str__
