""" A remote user backend
"""
import requests

from django.conf import settings
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import AllowAllUsersRemoteUserBackend

from wulf import jwt
from wulf.models import Status


def get_user_uri(request, uid):
    """ get the user info url
    """
    if 'wulf:' in settings.WULF_IDP_USER_URI:  # this is a local url
        path = reverse(settings.WULF_IDP_USER_URI, kwargs={'uid': uid})
        return request.build_absolute_uri(path)
    return settings.WULF_IDP_USER_URI.format(uid=uid)


class WulfIDPBackend(AllowAllUsersRemoteUserBackend):
    """ Remote JWT based authentication
    """
    def authenticate(self, request, jwt_str):
        head, body, errs = jwt.unpack_jwt(jwt_str)
        if errs:
            return None
        uri = get_user_uri(request, body.uid)
        resp = requests.get(uri, headers={'Authorization': f'JWT {jwt_str}'})
        if not resp.ok:
            return None
        resp = resp.json()
        user_model = get_user_model()
        # some magic here
        try:
            user = user_model.objects.get(user_id=body.uid)
        except user_model.DoesNotExist:
            key_field = user_model.USERNAME_FIELD
            user, _ = user_model(**{key_field: resp[key_field]})

        # always update the user
        for key, value in resp.items():
            setattr(user, key, value)
        user.save()
        # add this to the model so we can add an expiration time to the session cookie
        user._session_expires = jwt.unpack_timestamp(head.naf)
        return user


def activate_social_user(strategy, details, user=None, *args, **kwargs):
    """ Activate the social user who made it all the way throught the auth pipeline
    """
    if user:
        user.status = Status.active.value
        user.save()
