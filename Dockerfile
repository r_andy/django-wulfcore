FROM python:3.6
MAINTAINER andrew young <ayoung@thewulf.org>

RUN apt-get install -y libmysqlclient-dev

RUN mkdir /app
WORKDIR /app

ADD requirements.txt /app
ADD requirements.dev.txt /app
RUN pip install -r requirements.dev.txt
